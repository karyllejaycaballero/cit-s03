package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	private String contentType;
	
	public void init() throws ServletException{
		
		contentType ="index/html";
		System.out.println("**************************");
		System.out.println("CalculatorServlet has been initialized");
		System.out.println("**************************");
	}
	
	
	public void destroy() {
		System.out.println("**************************");
		System.out.println("CalculatorServlet has been destroyed.");
		System.out.println("**************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		System.out.println("Hello from the calculator servlet");
		
		double num1 = Integer.parseInt(req.getParameter("num1"));
		double num2 = Integer.parseInt(req.getParameter("num2"));
		String operation = req.getParameter("operation");
		
		double total = 0;
		
		if(req.getParameter("operation").equals("add")){
			total= num1+num2;
		}
		else if(req.getParameter("operation").equals("subtract")){
			total=num1-num2;	
		}
		else if(req.getParameter("operation").equals("multiply")){
			total=num1*num2;	
		}
		else if(req.getParameter("operation").equals("divide")){
			total=num1/num2;
		}
		

		PrintWriter out = res.getWriter();
		
		out.println("<p>The two numbers you provided are : "  + num1 + " , " + num2 + "</p>");
		out.println("<p>The operation that you wanted is : " + operation + "</p>");
		out.println("<p>The result is : " + total + "</p>");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		PrintWriter out = res.getWriter();
		
		out.println("<h1>You are now using the calculator app</h1><br><br>");
		out.println("To use the app, input two numbers and an operation.<br><br>");
		out.println("Hit the submit button after filling in the details.<br><br>");
		out.println("You will get the result shown in your browser!");
		
	}
	

}
